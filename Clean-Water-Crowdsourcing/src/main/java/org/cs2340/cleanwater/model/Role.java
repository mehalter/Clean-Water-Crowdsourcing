package main.java.org.cs2340.cleanwater.model;

public enum Role {
    USER("USER"),
    WORKER("WORKER"),
    MANAGER("MANAGER"),
    ADMIN("ADMIN");

    private final String value;

    /**
     * create enum from string
     *
     * @param value string of value
     */
    Role(String value) {
        this.value = value;
    }

    /**
     * get corresponding string
     *
     * @return the string matching the value
     */
    public String get() {
        return value;
    }
}
