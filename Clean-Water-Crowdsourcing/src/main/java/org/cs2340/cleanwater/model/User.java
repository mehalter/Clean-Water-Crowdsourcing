package main.java.org.cs2340.cleanwater.model;

/**
 * User Object
 */
public class User {
    private final String name;
    private final Role role;

    private String title = "";
    private String email = "";
    private String addr = "";

    /**
     * Creates a new user object
     *
     * @param name  user's name
     * @param role  user's role
     * @param title user's title
     * @param email user's email
     * @param addr  user's address
     */
    public User(String name, Role role, String title, String email, String addr) {
        this.name = name;
        this.role = role;
        this.title = title;
        this.email = email;
        this.addr = addr;
    }

    /**
     * Gets an account's profile information
     *
     * @return array of a user's information
     */
    public String[] getProfileInformation() {
        return new String[]{title, name, email, addr, role.toString()};
    }

    /**
     * Gets a user's name
     *
     * @return The user's name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets a user's role
     *
     * @return The user's role
     */
    public Role getRole() {
        return role;
    }
}
