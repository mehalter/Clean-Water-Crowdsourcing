package main.java.org.cs2340.cleanwater.model;

public enum OverallCondition {
    SAFE("SAFE"),
    TREATABLE("TREATABLE"),
    UNSAFE("UNSAFE");

    private final String value;

    /**
     * create enum from string
     *
     * @param value string of value
     */
    OverallCondition(String value) {
        this.value = value;
    }

    /**
     * get corresponding string
     *
     * @return the string matching the value
     */
    public String get() {
        return value;
    }
}
