package main.java.org.cs2340.cleanwater.model;

import java.time.LocalDateTime;

/**
 * Abstract class for information in both reports to minimize duplicate code
 */
public abstract class Report {
    private final LocalDateTime timestamp;
    private final String reporter;
    private final float lat;
    private final float lng;

    /**
     * Abstract constructor to set similar data
     *
     * @param reporter  name of reporter
     * @param timestamp created time
     * @param lat       location latitude
     * @param lng       location longitude
     */
    Report(String reporter, LocalDateTime timestamp, float lat, float lng) {
        this.reporter = reporter;
        this.timestamp = timestamp;
        this.lat = lat;
        this.lng = lng;
    }

    /**
     * get reporter name
     *
     * @return reporter name
     */
    public String getReporter() {
        return reporter;
    }

    /**
     * get location
     *
     * @return array containing latitude and longitude
     */
    public float[] getLocation() {
        return new float[]{lat, lng};
    }

    /**
     * get timestamp of report
     *
     * @return time of report
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * get report information
     *
     * @return array of rest of report information
     */
    abstract public String[] getInformation();
}
