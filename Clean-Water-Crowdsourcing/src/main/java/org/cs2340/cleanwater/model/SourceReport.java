package main.java.org.cs2340.cleanwater.model;

import java.time.LocalDateTime;

/**
 * Purity Report Object
 */
class SourceReport extends Report {
    private final Condition condition;
    private final Type type;

    /**
     * Creates a new purity report object
     *
     * @param reporter  name
     * @param timestamp created time
     * @param lat       location latitude
     * @param lng       location longitude
     * @param condition condition of water
     * @param type      type of water at location
     */
    SourceReport(String reporter, LocalDateTime timestamp, float lat, float lng, Condition condition, Type type) {
        super(reporter, timestamp, lat, lng);
        this.condition = condition;
        this.type = type;
    }

    @Override
    public String[] getInformation() {
        return new String[]{condition.get(), type.get()};
    }
}
