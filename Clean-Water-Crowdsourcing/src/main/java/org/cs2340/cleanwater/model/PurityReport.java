package main.java.org.cs2340.cleanwater.model;

import java.time.LocalDateTime;

/**
 * Purity Report Object
 */
class PurityReport extends Report {
    private final OverallCondition condition;
    private final int virusPPM;
    private final int contaminantPPM;

    /**
     * Creates a new purity report object
     *
     * @param reporter       name
     * @param timestamp      created time
     * @param lat            location latitude
     * @param lng            location longitude
     * @param condition      condition of water
     * @param virusPPM       virus PPM
     * @param contaminantPPM contaminant PPM
     */
    PurityReport(String reporter, LocalDateTime timestamp, float lat, float lng, OverallCondition condition, int virusPPM, int contaminantPPM) {
        super(reporter, timestamp, lat, lng);
        this.condition = condition;
        this.virusPPM = virusPPM;
        this.contaminantPPM = contaminantPPM;
    }

    @Override
    public String[] getInformation() {
        return new String[]{condition.get(), String.valueOf(virusPPM), String.valueOf(contaminantPPM)};
    }
}
