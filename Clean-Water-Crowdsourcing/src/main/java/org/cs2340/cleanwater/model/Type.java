package main.java.org.cs2340.cleanwater.model;

public enum Type {
    BOTTLE("BOTTLE"),
    WELL("WELL"),
    STREAM("STREAM"),
    LAKE("LAKE"),
    SPRING("SPRING"),
    OTHER("OTHER");

    private final String value;

    /**
     * create enum from string
     *
     * @param value string of value
     */
    Type(String value) {
        this.value = value;
    }

    /**
     * get corresponding string
     *
     * @return the string matching the value
     */
    public String get() {
        return value;
    }
}
