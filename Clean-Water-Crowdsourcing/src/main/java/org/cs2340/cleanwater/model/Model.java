package main.java.org.cs2340.cleanwater.model;

import javafx.collections.ObservableList;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Class for accessing and controlling MySQL based model
 */
@SuppressWarnings("SqlDialectInspection")
public class Model {

    private static final Model instance = new Model();
    private ObservableList<Report> historyReports;
    private boolean isVirusPPM;
    private int currentUser;

    public Model() {
        currentUser = 0;
        historyReports = null;
        isVirusPPM = false;
    }

    /**
     * Gets an instance of the current application
     *
     * @return the model with the current user's data
     */
    public static Model getInstance() {
        return instance;
    }

    /**
     * Get the current history report list
     *
     * @return the current model's history reports list
     */
    public ObservableList<Report> getHistoryReports() {
        return historyReports;
    }

    /**
     * set the model's history reports list
     *
     * @param historyReports the new history reports list that should be set
     */
    public void setHistoryReports(ObservableList<Report> historyReports) {
        this.historyReports = historyReports;
    }

    /**
     * check if model is set to virus or contaminant ppm
     *
     * @return true if virus ppm, false if contaminant
     */
    public boolean getIsVirusPPM() {
        return isVirusPPM;
    }

    /**
     * set virus or contaminant ppm setting
     *
     * @param isVirusPPM the new setting of the virus or contaminant ppm
     */
    public void setIsVirusPPM(boolean isVirusPPM) {
        this.isVirusPPM = isVirusPPM;
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://sql9.freemysqlhosting.net:3306/sql9142699?useSSL=true", "sql9142699", "kQH9jEbNxR");
    }

    /**
     * Gets the current logged in user
     *
     * @return the logged in user
     */
    public User getCurrentUser() {
        User out = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement userFromId = conn.prepareStatement("SELECT `name`, `role`, `title`, `email`, `address` FROM `Users` WHERE `id`=?");
            userFromId.setInt(1, currentUser);
            ResultSet result = userFromId.executeQuery();

            if (result.next())
                out = new User(result.getString(1),
                      Role.valueOf(result.getString(2)),
                      result.getString(3),
                      result.getString(4),
                      result.getString(5));

            conn.close();
        } catch (Exception ignored) {
        }

        return out;
    }

    /**
     * Retrieve all source reports from database
     *
     * @return ArrayList of source reports
     */
    public ArrayList<Report> getSourceReports() {
        ArrayList<Report> out = new ArrayList<>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement userFromPass = conn.prepareStatement("SELECT `u`.`name`, `s`.`created`, `s`.`lat`, `s`.`lng`, `s`.`condition`, type FROM `Sources` s LEFT JOIN `Users` u ON `s`.`user_id`=`u`.`id`");
            ResultSet result = userFromPass.executeQuery();

            while (result.next())
                out.add(new SourceReport(result.getString(1),
                      result.getTimestamp(2).toLocalDateTime(),
                      result.getFloat(3),
                      result.getFloat(4),
                      Condition.valueOf(result.getString(5)),
                      Type.valueOf(result.getString(6))));

            conn.close();
        } catch (Exception ignored) {
        }

        return out;
    }

    /**
     * Retrieve all purity reports from database
     *
     * @return ArrayList of purity reports
     */
    public ArrayList<Report> getPurityReports() {
        ArrayList<Report> out = new ArrayList<>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement userFromPass = conn.prepareStatement("SELECT `u`.`name`, `p`.`created`, `p`.`lat`, `p`.`lng`, `p`.`condition`, `p`.`virus_ppm`, `p`.`contaminant_ppm` FROM `Purities` p LEFT JOIN `Users` u ON `p`.`user_id`=`u`.`id`");
            ResultSet result = userFromPass.executeQuery();

            while (result.next())
                out.add(new PurityReport(result.getString(1),
                      result.getTimestamp(2).toLocalDateTime(),
                      result.getFloat(3),
                      result.getFloat(4),
                      OverallCondition.valueOf(result.getString(5)),
                      result.getInt(6),
                      result.getInt(7)));

            conn.close();
        } catch (Exception ignored) {
        }

        return out;
    }

    /**
     * Checks if a user is valid and
     * updates the model with the current user
     *
     * @param user The username of the current user
     * @param pass The password of the current user
     * @return True if the user is valid, false if not
     */
    public boolean setCurrentUser(String user, String pass) {
        boolean out = false;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement userFromPass = conn.prepareStatement("SELECT `id` FROM `Users` WHERE `username`=? AND `password`=?");
            userFromPass.setString(1, user);
            userFromPass.setString(2, hashPassword(pass));
            ResultSet result = userFromPass.executeQuery();

            if (result.next()) {
                currentUser = result.getInt(1);
                out = true;
            }

            conn.close();
        } catch (Exception ignored) {
        }

        return out;
    }

    /**
     * sets current user to no one
     */
    public void logOff() {
        currentUser = 0;
    }

    /**
     * Adds a user to the database
     *
     * @param name The name of the new user
     * @param user The username of the user
     * @param pass The password of the user
     * @param role The role of the user
     * @return True if the user is added, false if not
     */
    public boolean addUser(String name, String user, String pass, Role role) {
        boolean out = false;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement newUser = conn.prepareStatement("INSERT INTO `Users` (`username`, `password`, `role`, `name`) VALUES (?, ?, ?, ?)");
            newUser.setString(1, user);
            newUser.setString(2, hashPassword(pass));
            newUser.setString(3, role.toString());
            newUser.setString(4, name);

            if (newUser.executeUpdate() == 1)
                out = true;

            conn.close();
        } catch (Exception ignored) {
        }

        return out;
    }

    /**
     * Adds a new source report to database
     *
     * @param location  location of report
     * @param condition condition of water
     * @param type      type of water
     */
    public void addSourceReport(String location, Condition condition, Type type) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement newSource = conn.prepareStatement("INSERT INTO `Sources` (`user_id`, `lat`, `lng`, `condition`, `type`) VALUES (?, ?, ?, ?, ?, ?)");
            newSource.setInt(1, currentUser);
            String[] latLng = location.split(", ");
            newSource.setFloat(2, Float.parseFloat(latLng[0]));
            newSource.setFloat(3, Float.parseFloat(latLng[1]));
            newSource.setString(4, condition.toString());
            newSource.setString(5, type.toString());

            newSource.executeUpdate();

            conn.close();
        } catch (Exception ignored) {
        }
    }


    /**
     * Adds a new source report to database
     *
     * @param location       location of report
     * @param condition      condition of water
     * @param virusPPM       virus PPM
     * @param contaminantPPM contaminant PPM
     */
    public void addPurityReport(String location, OverallCondition condition, int virusPPM, int contaminantPPM) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement newPurity = conn.prepareStatement("INSERT INTO `Purities` (`user_id`, `lat`, `lng`, `condition`, `virus_ppm`, `contaminant_ppm`) VALUES (?, ?, ?, ?, ?, ?)");
            newPurity.setInt(1, currentUser);
            String[] latLng = location.split(", ");
            newPurity.setFloat(2, Float.parseFloat(latLng[0]));
            newPurity.setFloat(3, Float.parseFloat(latLng[1]));
            newPurity.setString(4, condition.toString());
            newPurity.setInt(5, virusPPM);
            newPurity.setInt(6, contaminantPPM);

            newPurity.executeUpdate();

            conn.close();
        } catch (Exception ignored) {
        }
    }

    private String hashPassword(String pass) throws NoSuchAlgorithmException {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-256");
        md.update(pass.getBytes(StandardCharsets.UTF_8));
        return DatatypeConverter.printHexBinary(md.digest());
    }

    /**
     * Updates the current user's information in the database
     *
     * @param role  updated role
     * @param name  updated name
     * @param title updated title
     * @param email updated email
     * @param addr  updated address
     */
    public void updateUser(Role role, String name, String title, String email, String addr) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = getConnection();

            PreparedStatement updateUser = conn.prepareStatement("UPDATE `Users` SET `role`=?, `name`=?, `title`=?, `email`=?, `address`=? WHERE `id`=?");
            updateUser.setString(1, role.toString());
            updateUser.setString(2, name);
            updateUser.setString(3, title);
            updateUser.setString(4, email);
            updateUser.setString(5, addr);
            updateUser.setInt(6, currentUser);

            updateUser.executeUpdate();

            conn.close();
        } catch (Exception ignored) {
        }
    }
}
