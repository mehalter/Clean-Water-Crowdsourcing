package main.java.org.cs2340.cleanwater.model;

/**
 * Enum of possible water conditions in source reports
 */
public enum Condition {
    WASTE("WASTE"),
    CLEAR("CLEAR"),
    MUDDY("MUDDY"),
    POTABLE("POTABLE");

    private final String value;

    /**
     * create enum from string
     *
     * @param value string of value
     */
    Condition(String value) {
        this.value = value;
    }

    /**
     * get corresponding string
     *
     * @return the string matching the value
     */
    public String get() {
        return value;
    }
}
