-- ---
-- Globals
-- ---

DROP TABLE IF EXISTS `Purities`;
DROP TABLE IF EXISTS `Sources`;
DROP TABLE IF EXISTS `Users`;

-- ---
-- Table 'Users'
-- 
-- ---

DROP TABLE IF EXISTS `Users`;
		
CREATE TABLE `Users` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` VARCHAR(255) NOT NULL,
  `password` CHAR(64) NOT NULL,
  `role` ENUM('USER', 'WORKER', 'MANAGER', 'ADMIN') NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `address` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`username`)
);

-- ---
-- Table 'Sources'
-- 
-- ---

DROP TABLE IF EXISTS `Sources`;
		
CREATE TABLE `Sources` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` INTEGER NOT NULL,
  `lat` DECIMAL(10, 8) NOT NULL,
  `lng` DECIMAL(11, 8) NOT NULL,
  `condition` ENUM('WASTE', 'CLEAR', 'MUDDY', 'POTABLE') NOT NULL,
  `type` ENUM('BOTTLE', 'WELL', 'STREAM', 'LAKE', 'SPRING', 'OTHER') NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Purities'
-- 
-- ---

DROP TABLE IF EXISTS `Purities`;
		
CREATE TABLE `Purities` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` INTEGER NOT NULL,
  `lat` DECIMAL(10, 8) NOT NULL,
  `lng` DECIMAL(11, 8) NOT NULL,
  `condition` ENUM('SAFE', 'TREATABLE', 'UNSAFE') NOT NULL,
  `virus_ppm` INT NOT NULL,
  `contaminant_ppm` INT NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `Sources` ADD FOREIGN KEY (user_id) REFERENCES `Users` (`id`);
ALTER TABLE `Purities` ADD FOREIGN KEY (user_id) REFERENCES `Users` (`id`);

-- ---
-- Test Data
-- ---

INSERT INTO `Users` (`username`,`password`,`role`,`name`,`title`,`email`,`address`) VALUES
  ('mehalter','C501F67D7CD52CF1626AFF30752B3A55F5167295F21735C3D513EAAD89B465F4','MANAGER','Micah','Mr','micah@mehalter.com','5601 Bedfordshire Ave. Harrisburg, NC 28075');

INSERT INTO `Sources` (`user_id`, `lat`, `lng`, `condition`, `type`) VALUES
  (1, 47.6397, -122.3031, 'CLEAR', 'SPRING');

INSERT INTO `Purities` (`user_id`, `lat`, `lng`, `condition`, `virus_ppm`, `contaminant_ppm`) VALUES
  (1, 47.6397, -122.3031, 'SAFE', 123, 150);
