package main.java.org.cs2340.cleanwater.model;

public enum PPM {
    VIRUS("VIRUS"),
    CONTAMINANT("CONTAMINANT");

    private final String value;

    PPM(String value) {
        this.value = value;
    }

    public String get() {
        return value;
    }
}
