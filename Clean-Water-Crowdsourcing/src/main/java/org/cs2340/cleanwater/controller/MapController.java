package main.java.org.cs2340.cleanwater.controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.fxml.FXML;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Report;
import netscape.javascript.JSObject;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Controller for map window to display all source reports
 */
public class MapController extends Controller implements MapComponentInitializedListener {

    @FXML
    private GoogleMapView mapView;

    private GoogleMap map;

    @FXML
    private void initialize() {
        mapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized() {

        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(47.6097, -122.3331))
              .mapType(MapTypeIdEnum.ROADMAP)
              .zoom(12);

        map = mapView.createMap(mapOptions);

        ArrayList<Report> sourceReports = Model.getInstance().getSourceReports();
        for (Report r : sourceReports) {
            float[] l = r.getLocation();
            LatLong latLong = new LatLong(l[0], l[1]);

            MarkerOptions markerOption = new MarkerOptions();
            markerOption.position(latLong);

            Marker marker = new Marker(markerOption);
            map.addMarker(marker);


            String[] sourceInfo = r.getInformation();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm MM-dd-yyyy");
            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            infoWindowOptions.content("<h2>" + l[0] + ", " + l[1] + "</h2>"
                  + "Submitted On: " + r.getTimestamp().format(formatter) + "<br>"
                  + "Submitted By: " + r.getReporter() + "<br>"
                  + "Condition: " + sourceInfo[0] + "<br>"
                  + "Type: " + sourceInfo[1]);

            InfoWindow markerWindow = new InfoWindow(infoWindowOptions);

            map.addUIEventHandler(marker, UIEventType.click, (JSObject obj) -> {
                if (markerWindow.getPosition() == null)
                    markerWindow.open(map, marker);
                else {
                    markerWindow.close();
                    markerWindow.setPosition(null);
                }
            });
        }
    }
}
