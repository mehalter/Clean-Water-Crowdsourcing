package main.java.org.cs2340.cleanwater.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Report;

import java.time.Month;

/**
 * Controller for History Chart page
 */
public class HistoryChartController extends Controller {

    @FXML
    private LineChart<String, Number> historyChart;

    @FXML
    private void initialize() {
        historyChart.setLegendVisible(false);

        historyChart.getXAxis().setLabel("Month");
        historyChart.getYAxis().setLabel((Model.getInstance().getIsVirusPPM() ? "Virus" : "Contaminant") + " PPM");

        float[][] data = new float[12][2];
        ObservableList<Report> reportList = Model.getInstance().getHistoryReports();
        for (Report report : reportList) {
            int month = report.getTimestamp().getMonthValue() - 1;
            data[month][0] += Float.valueOf(report.getInformation()[Model.getInstance().getIsVirusPPM() ? 1 : 2]);
            data[month][1]++;
        }

        XYChart.Series<String, Number> series = new XYChart.Series<>();
        for (int i = 0; i < data.length; i++)
            series.getData().add(new XYChart.Data<>(Month.of(i + 1).toString(), data[i][1] == 0 ? 0 : data[i][0] / data[i][1]));

        historyChart.getData().add(series);
    }
}
