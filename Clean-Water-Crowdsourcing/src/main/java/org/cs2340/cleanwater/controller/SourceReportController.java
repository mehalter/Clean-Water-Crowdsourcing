package main.java.org.cs2340.cleanwater.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import main.java.org.cs2340.cleanwater.model.Condition;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Type;

import static main.java.org.cs2340.cleanwater.controller.Utils.*;

/**
 * Controller for page where you submit new source report
 */
public class SourceReportController extends Controller {

    @FXML
    private TextField locationField;

    @FXML
    private ComboBox<String> typeField;

    @FXML
    private ComboBox<String> conditionField;

    @FXML
    private void initialize() {
        typeField.getItems().clear();
        typeField.setItems(getWaterTypes());

        conditionField.getItems().clear();
        conditionField.setItems(getWaterConditions());
    }

    @FXML
    private void handleSubmitPressed() {
        String location = locationField.getText();
        if (validateReport(location, new Object[]{conditionField.getValue(), typeField.getValue()})) {
            Condition waterCondition = Condition.valueOf(conditionField.getValue());
            Type waterType = Type.valueOf(typeField.getValue());

            Model.getInstance().addSourceReport(location, waterCondition, waterType);

            dialogStage.close();
        }
    }

    @FXML
    private void handleCancelPressed() {
        dialogStage.close();
    }

}
