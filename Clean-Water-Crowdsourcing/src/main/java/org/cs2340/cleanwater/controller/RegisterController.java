package main.java.org.cs2340.cleanwater.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Role;

import static main.java.org.cs2340.cleanwater.controller.Utils.getRoleValues;
import static main.java.org.cs2340.cleanwater.controller.Utils.showAlert;

/**
 * Controller for page where new users can register
 */
public class RegisterController extends Controller {

    @FXML
    private TextField nameField;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private ComboBox<String> roleField;

    @FXML
    private void initialize() {
        roleField.getItems().clear();
        roleField.setItems(getRoleValues());
        roleField.setValue(Role.USER.get());
    }

    @FXML
    private void handleRegisterPressed() {
        String name = nameField.getText();
        String pass = passwordField.getText();
        String user = usernameField.getText();
        Role role = Role.valueOf(roleField.getValue());
        if (Model.getInstance().addUser(name, user, pass, role))
            dialogStage.close();
        else
            showAlert("Registration Error", "User Exists", "The username has already been taken.");
    }

    @FXML
    private void handleCancelPressed() {
        dialogStage.close();
    }
}
