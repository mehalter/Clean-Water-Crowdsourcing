package main.java.org.cs2340.cleanwater.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.PPM;
import main.java.org.cs2340.cleanwater.model.Report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static main.java.org.cs2340.cleanwater.controller.Utils.*;

/**
 * Controller for page requesting a history report
 */
public class HistoryReportController extends Controller {

    @FXML
    private TextField locationField;

    @FXML
    private TextField yearField;

    @FXML
    private ComboBox<String> ppmField;

    private boolean isSubmitPressed = false;

    boolean getIsSubmitPressed() {
        return isSubmitPressed;
    }

    @FXML
    private void initialize() {
        yearField.textProperty().addListener(numbersOnlyListener(yearField));
        ppmField.getItems().clear();
        ppmField.setItems(getPPMTypes());
        ppmField.setValue(PPM.VIRUS.get());
    }

    @FXML
    private void handleSubmitPressed() {
        if (checkLocation(locationField.getText())) {
            if (ppmField.getValue() != null && !yearField.getText().equalsIgnoreCase("")) {
                String[] locationText = locationField.getText().split(", ");
                System.out.println(locationText[0]);
                float[] location = new float[]{Float.parseFloat(locationText[0]), Float.parseFloat(locationText[1])};
                ArrayList<Report> reportList = Model.getInstance().getPurityReports();
                List<Report> reports = reportList.stream().filter(report ->
                      Arrays.equals(report.getLocation(), location) && report.getTimestamp().getYear() == Integer.valueOf(yearField.getText()))
                      .collect(Collectors.toList());
                Model.getInstance().setHistoryReports(FXCollections.observableArrayList(reports));
                Model.getInstance().setIsVirusPPM(ppmField.getValue().equalsIgnoreCase("VIRUS"));
                isSubmitPressed = true;
                dialogStage.close();
            } else
                showAlert("Information Error", "Incomplete Form", "You must fill in every part of the form");
        } else
            showAlert("Location Error", "Incorrect Location Format", "Please input location as 'lat, long'");
    }

    @FXML
    private void handleCancelPressed() {
        dialogStage.close();
    }
}
