package main.java.org.cs2340.cleanwater.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Report;

import java.util.ArrayList;

/**
 * Controller where you can view all source reports
 */
public class SourceListController extends Controller {

    @FXML
    private ListView<String> listView;

    @FXML
    private void initialize() {
        ObservableList<String> data = FXCollections.observableArrayList();
        ArrayList<Report> reports = Model.getInstance().getSourceReports();
        for (Report r : reports) {
            String[] s = r.getInformation();
            float[] l = r.getLocation();
            data.add("Location: (" + l[0] + ", " + l[1] + ")   By: " + r.getReporter() + "   Condition: " + s[0] + "   Type: " + s[1]);
        }
        listView.setItems(data);
    }
}
