package main.java.org.cs2340.cleanwater.controller;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import main.java.org.cs2340.cleanwater.model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class of general functions used throughout the controller classes
 */
public class Utils {

    /**
     * Gets a list of account roles
     *
     * @return A list of the account roles
     */
    static ObservableList<String> getRoleValues() {
        List<String> roles = Arrays.stream(Role.values()).map(Role::get).collect(Collectors.toList());
        return FXCollections.observableList(roles);
    }

    /**
     * Gets a list of PPM types
     *
     * @return A list of the PPM types
     */
    static ObservableList<String> getPPMTypes() {
        List<String> ppms = Arrays.stream(PPM.values()).map(PPM::get).collect(Collectors.toList());
        return FXCollections.observableList(ppms);
    }

    /**
     * Gets a list of water types
     *
     * @return A list of water types
     */
    static ObservableList<String> getWaterTypes() {
        List<String> types = Arrays.stream(Type.values()).map(Type::get).collect(Collectors.toList());
        return FXCollections.observableList(types);
    }

    /**
     * Gets a list of water conditions
     *
     * @return A list of water conditions
     */
    static ObservableList<String> getWaterConditions() {
        List<String> conditions = Arrays.stream(Condition.values()).map(Condition::get).collect(Collectors.toList());
        return FXCollections.observableList(conditions);
    }


    /**
     * Gets a list of overall water conditions
     *
     * @return A list of water conditions
     */
    static ObservableList<String> getOverallConditions() {
        List<String> conditions = Arrays.stream(OverallCondition.values()).map(OverallCondition::get).collect(Collectors.toList());
        return FXCollections.observableList(conditions);
    }


    /**
     * Shows a JavaFX Alert window displaying information of an error
     *
     * @param title   Title of the error
     * @param header  Header of the error message
     * @param message Full details of the encountered error
     */
    static void showAlert(String title, String header, String message) {
        try {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(title);
            alert.setHeaderText(header);
            alert.setContentText(message);

            alert.showAndWait();
        /*
        Catch blocks for when run with JUnit Tests and no
        JavaFx instance is running
         */
        } catch (ExceptionInInitializerError | NoClassDefFoundError ignored) {
        }
    }

    /**
     * Checks to make sure information in form is valid
     *
     * @param location lat, lng string provided in form
     * @param objs     all other indicies of the form that are being checked
     * @return true if lat, lng string is formatted correctly and all objects are not null or empty strings, false if otherwise
     */
    public static boolean validateReport(String location, Object[] objs) {
        // Validates location string format
        boolean locMatch = checkLocation(location);
        boolean notNull = true;

        // Makes sure all objects aren't null or empty strings
        for (int i = 0; i < objs.length && notNull; i++)
            if (objs[i] == null ||
                  (objs[i] instanceof String && ((String) objs[i]).equalsIgnoreCase("")))
                notNull = false;

        // If there is an error, display correct error message
        if (!locMatch)
            showAlert("Location Error", "Incorrect Location Format", "Please input location as 'lat, long'");
        else if (!notNull)
            showAlert("Information Error", "Incomplete Form", "You must fill in every part of the form");

        // return boolean of validity to call
        return notNull && locMatch;
    }

    /**
     * Verifies location lat, lng string
     *
     * @param location the lat, lng string being checked
     * @return true if valid format, false otherwise
     */
    static boolean checkLocation(String location) {
        // regex to check lat, lng string
        return location.matches("^(-?\\d+(.\\d+)?),\\s*(-?\\d+(.\\d+)?)$");
    }

    /**
     * Listener for number input fields so that non-numbers can't be typed
     *
     * @param textField the text field the listener is being added to
     * @return the newly created listener
     */
    static ChangeListener<String> numbersOnlyListener(TextField textField) {
        return (observable, oldValue, newValue) -> {
            // If non-number is typed insert nothing
            if (!newValue.matches("\\d*"))
                textField.setText(newValue.replaceAll("[^\\d]", ""));
        };
    }

    /**
     * Method that generalizes the method that loads a JavaFX page and puts it to the screen
     *
     * @param url   the location of the page
     * @param title the title of the window
     * @return the controller running to check metadata of page once it is closed
     */
    public static Controller showAndWait(URL url, String title) {
        Controller control = null;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(url);
            Pane page = loader.load();
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(page));
            stage.setResizable(false);

            control = loader.getController();
            control.setDialogStage(stage);

            stage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return control;
    }
}
