package main.java.org.cs2340.cleanwater.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Role;

import static main.java.org.cs2340.cleanwater.controller.Utils.showAndWait;

/**
 * Controller of main welcome page to access all other pages
 */
public class MainController extends Controller {

    @FXML
    private Label welcomeText;

    @FXML
    private Button sourceListButton;

    @FXML
    private Button purityButton;

    @FXML
    private Button purityListButton;

    @FXML
    private Button historyButton;

    private boolean loggedOff = false;

    @FXML
    private void initialize() {
        welcomeText.setText("Welcome " + Model.getInstance().getCurrentUser().getName() + "!");

        sourceListButton.setVisible(false);
        purityButton.setVisible(false);
        purityListButton.setVisible(false);
        historyButton.setVisible(false);

        Role currentRole = Model.getInstance().getCurrentUser().getRole();

        if (currentRole == Role.MANAGER || currentRole == Role.WORKER) {
            purityButton.setVisible(true);
            if (Model.getInstance().getCurrentUser().getRole() == Role.MANAGER) {
                purityListButton.setVisible(true);
                sourceListButton.setVisible(true);
                historyButton.setVisible(true);
            }
        }
    }

    @FXML
    private void editPressed() {
        showEdit();
    }

    @FXML
    private void sourcePressed() {
        showSourceReport();
    }

    @FXML
    private void purityPressed() {
        showPurityReport();
    }

    @FXML
    private void mapPressed() {
        showMap();
    }

    @FXML
    private void historyPressed() {
        showHistoryReport();
    }

    @FXML
    private void sourceListPressed() {
        showSourceList();
    }

    @FXML
    private void purityListPressed() {
        showPurityList();
    }

    private void showEdit() {
        showAndWait(MainController.class.getResource("../view/Profile.fxml"), "Edit Profile");
        initialize();
    }

    private void showSourceReport() {
        showAndWait(MainController.class.getResource("../view/SourceReport.fxml"), "Submit Source Report");
    }

    private void showPurityReport() {
        showAndWait(MainController.class.getResource("../view/PurityReport.fxml"), "Submit Purity Report");
    }

    private void showMap() {
        showAndWait(MainController.class.getResource("../view/Map.fxml"), "Map");
    }

    private void showHistoryReport() {
        HistoryReportController controller = (HistoryReportController) showAndWait(MainController.class.getResource("../view/HistoryReport.fxml"), "History Report Request");
        if (controller.getIsSubmitPressed())
            showHistoryChart();
    }

    private void showHistoryChart() {
        showAndWait(MainController.class.getResource("../view/HistoryChart.fxml"), "History Chart");
    }

    private void showSourceList() {
        showAndWait(MainController.class.getResource("../view/SourceList.fxml"), "View Reports");
    }

    private void showPurityList() {
        showAndWait(MainController.class.getResource("../view/PurityList.fxml"), "View Reports");
    }


    @FXML
    private void logOffPressed() {
        Model.getInstance().logOff();
        loggedOff = true;
        dialogStage.close();

    }

    /**
     * Confirms that a user is logged off
     *
     * @return True if a user is logged off, false if not
     */
    public boolean isLoggedOff() {
        return loggedOff;
    }

}
