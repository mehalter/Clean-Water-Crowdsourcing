package main.java.org.cs2340.cleanwater.controller;

import javafx.stage.Stage;

/**
 * Blanket controller abstract class to minimize duplicate code
 */
public abstract class Controller {

    Stage dialogStage;

    /**
     * Sets the dialogStage so a controller can easily close its own window
     *
     * @param dialogStage The window that the controller is running in
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
