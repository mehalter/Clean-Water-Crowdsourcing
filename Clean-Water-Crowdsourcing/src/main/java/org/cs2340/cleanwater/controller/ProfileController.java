package main.java.org.cs2340.cleanwater.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.Role;

import static main.java.org.cs2340.cleanwater.controller.Utils.getRoleValues;

/**
 * Controller for page where profile information is viewed and edited
 */
public class ProfileController extends Controller {

    @FXML
    private TextField titleField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField addrField;

    @FXML
    private ComboBox<String> roleField;

    @FXML
    private void initialize() {
        roleField.getItems().clear();
        roleField.setItems(getRoleValues());
        String[] userInfo = Model.getInstance().getCurrentUser().getProfileInformation();

        titleField.setText(userInfo[0]);
        nameField.setText(userInfo[1]);
        emailField.setText(userInfo[2]);
        addrField.setText(userInfo[3]);
        roleField.setValue(userInfo[4]);
    }

    @FXML
    private void handleSavePressed() {
        String title = titleField.getText();
        String name = nameField.getText();
        String email = emailField.getText();
        String addr = addrField.getText();
        Role role = Role.valueOf(roleField.getValue());
        Model.getInstance().updateUser(role, name, title, email, addr);
        dialogStage.close();
    }

    @FXML
    private void handleCancelPressed() {
        dialogStage.close();
    }
}
