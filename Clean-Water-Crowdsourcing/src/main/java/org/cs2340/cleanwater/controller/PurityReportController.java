package main.java.org.cs2340.cleanwater.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import main.java.org.cs2340.cleanwater.model.Model;
import main.java.org.cs2340.cleanwater.model.OverallCondition;

import static main.java.org.cs2340.cleanwater.controller.Utils.*;

/**
 * Controller for page where you submit new purity report
 */
public class PurityReportController extends Controller {

    @FXML
    private TextField locationField;

    @FXML
    private ComboBox<String> conditionField;

    @FXML
    private TextField virusField;

    @FXML
    private TextField contaminantField;

    @FXML
    private void initialize() {
        virusField.textProperty().addListener(numbersOnlyListener(virusField));
        contaminantField.textProperty().addListener(numbersOnlyListener(contaminantField));
        conditionField.getItems().clear();
        conditionField.setItems(getOverallConditions());
    }

    @FXML
    private void handleSubmitPressed() {
        String location = locationField.getText();
        if (validateReport(location, new Object[]{conditionField.getValue(), virusField.getText(), contaminantField.getText()})) {
            OverallCondition overallCondition = OverallCondition.valueOf(conditionField.getValue());
            int virusPPM = Integer.parseInt(virusField.getText());
            int contaminantPPM = Integer.parseInt(contaminantField.getText());

            Model.getInstance().addPurityReport(location, overallCondition, virusPPM, contaminantPPM);

            dialogStage.close();
        }
    }

    @FXML
    private void handleCancelPressed() {
        dialogStage.close();
    }
}
