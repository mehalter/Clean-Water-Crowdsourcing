package main.java.org.cs2340.cleanwater.controller;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.org.cs2340.cleanwater.model.Model;

import static main.java.org.cs2340.cleanwater.controller.Utils.showAlert;
import static main.java.org.cs2340.cleanwater.controller.Utils.showAndWait;

public class LoginController extends Controller {

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    private boolean isOkPressed = false;

    /**
     * Checks if the user has prompted the login screen to continue
     *
     * @return True if the user has pressed "Ok", false if not
     */
    public boolean getIsOkPressed() {
        return isOkPressed;
    }

    @FXML
    private void handleLoginPressed() {
        if (Model.getInstance().setCurrentUser(usernameField.getText(), passwordField.getText())) {
            isOkPressed = true;
            dialogStage.close();
        } else
            showAlert("Login Error", "Username or Password Error", "Username/Password don't match.");
    }

    @FXML
    private void handleRegisterPressed() {
        showRegister();
    }

    private void showRegister() {
        showAndWait(LoginController.class.getResource("../view/Register.fxml"), "Register");
    }
}
