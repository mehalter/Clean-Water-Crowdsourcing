package main.java.org.cs2340.cleanwater.fxapp;

import javafx.application.Application;
import javafx.stage.Stage;
import main.java.org.cs2340.cleanwater.controller.LoginController;
import main.java.org.cs2340.cleanwater.controller.MainController;

import static main.java.org.cs2340.cleanwater.controller.Utils.showAndWait;

/**
 * Class to initialize program and start it up
 */
public class Main extends Application {

    /**
     * Initializes program
     *
     * @param args command line arguments passed in
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        if (showLogin() && showMain())
            start(primaryStage);
    }

    /**
     * Creates and runs the main stage
     *
     * @return False if the window is closed or true if the user logs out
     */
    private boolean showMain() {
        MainController controller = (MainController) showAndWait(Main.class.getResource("../view/Main.fxml"), "Clean Water Crowdsourcing");
        return controller.isLoggedOff();
    }

    /**
     * Creates the login screen
     *
     * @return False if the window is closed or true if the user prompts another screen
     */
    private boolean showLogin() {
        LoginController controller = (LoginController) showAndWait(Main.class.getResource("../view/Login.fxml"), "Clean Water Crowdsourcing");
        return controller.getIsOkPressed();
    }
}
