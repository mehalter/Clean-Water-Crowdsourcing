package test.java.org.cs2340.cleanwater.model;

import main.java.org.cs2340.cleanwater.model.*;
import org.junit.Test;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * JUnit Tests for Model file
 */
public class ModelTest {

    /**
     * Tests to make sure getCurrentUser function is correctly validating all cases
     */
    @Test
    public void testGetCurrentUser() {
        Model m = new Model();
        assertNull("Case where no one has logged in, should be null", m.getCurrentUser());
        m.setCurrentUser("micah", "micah123");
        assertNotNull("Case where user has logged in, should not be null", m.getCurrentUser());
    }

    /**
     * Tests to see if setCurrentUser function is correctly setting user
     */
    @Test
    public void testSetCurrentUser() {
        Model m = new Model();
        assertTrue("Case: setting current user to valid user.", m.setCurrentUser("caitlyn", "caitlyn123"));
        assertFalse("Case: setting current user to invalid user.", m.setCurrentUser("cbritt30", "test"));
    }

    /**
     * Tests to see if testUpdateUser function is correctly updating user information
     */
    @Test
    public void testUpdateUser() {
        Model model = new Model();
        model.setCurrentUser("raymond", "password");
        User user = model.getCurrentUser();
        model.updateUser(Role.USER, "Raymond", "Mr.", "rzhang339@gmail.com", "1500 Asheforde Dr");
        String[] expectedProfileInfo = new String[]{"Sir", "Raymond", "abc@hotmail.com", "100 First Dr", "ADMIN"};
        model.updateUser(Role.ADMIN, "Raymond", "Sir", "abc@hotmail.com", "100 First Dr");
        assertEquals(expectedProfileInfo[0], model.getCurrentUser().getProfileInformation()[0]);
        assertEquals(expectedProfileInfo[1], model.getCurrentUser().getProfileInformation()[1]);
        assertEquals(expectedProfileInfo[2], model.getCurrentUser().getProfileInformation()[2]);
        assertEquals(expectedProfileInfo[3], model.getCurrentUser().getProfileInformation()[3]);
        assertEquals(expectedProfileInfo[4], model.getCurrentUser().getProfileInformation()[4]);
    }

    /**
     * Tests to see if getSourceReport correctly receives the information from a source report
     */
    @Test
    public void testGetSourceReport() {
        Model model = new Model();
        model.setCurrentUser("raymond", "password");
        float lat1 = (float) 47.6397;
        float lng1 = (float) -122.3031;

        String[] expectedInfo1 = new String[]{"Micah", "CLEAR", "SPRING"};

        assertEquals(model.getSourceReports().get(0).getReporter(), expectedInfo1[0]);
        assertEquals(model.getSourceReports().get(0).getInformation()[0], expectedInfo1[1]);
        assertEquals(model.getSourceReports().get(0).getInformation()[1], expectedInfo1[2]);
        assertEquals(model.getSourceReports().get(0).getLocation()[0], lat1, 0);
        assertEquals(model.getSourceReports().get(0).getLocation()[1], lng1, 0);
    }

}
