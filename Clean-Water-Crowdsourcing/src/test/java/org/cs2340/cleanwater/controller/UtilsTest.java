package test.java.org.cs2340.cleanwater.controller;

import main.java.org.cs2340.cleanwater.model.Condition;
import org.junit.Test;

import static main.java.org.cs2340.cleanwater.controller.Utils.validateReport;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * JUnit Tests for Utils file
 */
public class UtilsTest {

    /**
     * Tests to make sure validateReport function is correctly validating all cases
     */
    @Test
    public void testValidateReport() {
        String validLocation = "47.6197, -122.3231";
        String invalidLocation = "Not Valid Location";
        Condition object = Condition.CLEAR;
        Object nullObject = null;
        String numString = "123";
        String emptyString = "";

        //No errors in form
        assertTrue("Base case with valid location, no empty strings, and no null objects",
              validateReport(validLocation, new Object[]{object, numString}));

        //Single error in form
        assertFalse("Invalid report location",
              validateReport(invalidLocation, new Object[]{object, numString}));

        assertFalse("Null object in report",
              validateReport(validLocation, new Object[]{nullObject, numString}));

        assertFalse("Empty string in report",
              validateReport(validLocation, new Object[]{object, emptyString}));

        //Two errors in form
        assertFalse("Invalid report location and null object",
                validateReport(invalidLocation, new Object[]{nullObject, numString}));

        assertFalse("Invalid report location and empty string",
                validateReport(invalidLocation, new Object[]{object, emptyString}));

        assertFalse("Null object and empty string",
                validateReport(validLocation, new Object[]{nullObject, emptyString}));

        //Only errors in form
        assertFalse("Invalid report location, null object, and empty string",
                validateReport(invalidLocation, new Object[]{nullObject, emptyString}));
    }
}
